package app.business.controllers.rest;

import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import app.business.services.GroupMembershipService;
import app.business.services.OrganizationMembershipService;
import app.business.services.UserPhoneNumberService;
import app.business.services.UserService;
import app.entities.GroupMembership;
import app.entities.OrganizationMembership;
import app.entities.User;
import app.entities.UserPhoneNumber;

@RestController
@RequestMapping("/app")
public class MaintenanceController {

	@Autowired
	UserService userService;
	
	@Autowired
	UserPhoneNumberService userPhoneNumberService;
	
	@Autowired
	GroupMembershipService groupMembershipService;
	
	@Autowired
	OrganizationMembershipService organizationMembershipService;
	
	@RequestMapping(value = "/deleteallusers", method = RequestMethod.GET)
	public String checkVersionAdmin() {	
		List <User> list = userService.getAllUserList();
		Iterator <User> iterator = list.iterator();
		JSONObject responseJsonObject = new JSONObject();
		while(iterator.hasNext()){
			int flag =0;
			User user = iterator.next();
			List<OrganizationMembership> organizationMembershipList = organizationMembershipService.getOrganizationMembershipListByUser(user);
			Iterator <OrganizationMembership> membershipIter = organizationMembershipList.iterator();
			while (membershipIter.hasNext())
			{
				OrganizationMembership organizationMembership = membershipIter.next();
				if (organizationMembership.getIsAdmin() == false) {
					organizationMembershipService.removeOrganizationMembership(organizationMembership);
				}
				else {
					flag=1;
				}
			}
			if (flag == 0) {
				//not an admin
				int userId = user.getUserId();
				List <GroupMembership> groupList = groupMembershipService.getGroupMembershipListByUser(user);
				List <UserPhoneNumber> userPhoneNumberList = userPhoneNumberService.getAllUserPhoneNumberList(user);
				Iterator <UserPhoneNumber> phoneNumberIter = userPhoneNumberList.iterator();
				while (phoneNumberIter.hasNext())
					userPhoneNumberService.removeUserPhoneNumber(phoneNumberIter.next());
				
				Iterator <GroupMembership> groupMembershipIter = groupList.iterator();
				while (groupMembershipIter.hasNext())
					groupMembershipService.removeGroupMembership(groupMembershipIter.next());
				
				userService.removeUser(user);
				System.out.println("User: "+userId+" details has been removed");
				
			}	
			
		}
		

		/*
		while(iterator.hasNext()) {
			OrganizationMembership organizationMembership = iterator.next();
			if (organizationMembership.getIsAdmin() == false) {
				//Sparing the admins
				User user = organizationMembership.getUser();
				int userid = user.getUserId();
				List <UserPhoneNumber> phoneNumberList = userPhoneNumberService.getAllUserPhoneNumberList(user);
				List <GroupMembership> groupMembershipList = groupMembershipService.getGroupMembershipListByUser(user);
				organizationMembershipService.removeOrganizationMembership(organizationMembership);
				Iterator <UserPhoneNumber> phoneNumberIter = phoneNumberList.iterator();
				while (phoneNumberIter.hasNext())
					userPhoneNumberService.removeUserPhoneNumber(phoneNumberIter.next());
				
				Iterator <GroupMembership> groupMembershipIter = groupMembershipList.iterator();
				while (groupMembershipIter.hasNext())
					groupMembershipService.removeGroupMembership(groupMembershipIter.next());
				
			}
			userService.removeUser(user);
			
			System.out.println("User: "+userid+" details has been removed");
			
		}
		*/
		try {
			responseJsonObject.put("response", "success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
}
