package app.business.controllers.rest;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.plivo.helper.api.client.RestAPI;
import com.plivo.helper.api.response.message.MessageResponse;
import com.plivo.helper.exception.PlivoException;

import app.business.services.GroupMembershipService;
import app.business.services.OrganizationMembershipService;
import app.business.services.OrganizationService;
import app.business.services.ReferralService;
import app.business.services.SmsApiKeysService;
import app.business.services.UserPhoneNumberService;
import app.business.services.UserService;
import app.data.repositories.OrganizationMembershipRepository;
import app.entities.Organization;
import app.entities.OrganizationMembership;
import app.entities.Referral;
import app.entities.SmsApiKeys;
import app.entities.User;
import app.entities.UserPhoneNumber;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@RestController
@RequestMapping("/api")
public class ReferralRestController {

	@Autowired
	UserPhoneNumberService userPhoneNumberService;

	@Autowired
	UserService userService;

	@Autowired
	ReferralService referralService;

	@Autowired
	OrganizationService organizationService;

	@Autowired
	SmsApiKeysService smsApiKeysService;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	OrganizationMembershipService organizationMembershipService;
	
	@Autowired
	OrganizationMembershipRepository organizationMemberRepository;
	
	@Autowired
	GroupMembershipService groupMembershipService;
	
	@RequestMapping(value = "/refer", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String newReferral(@RequestBody String requestBody) {

		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject = null, branchResponse = null;
		String phonenumber = null, email = null, orgabbr = null, refreeEmail = null, password=null, generatedUrl = null, address = null, name = null, lastname = null;
		Random random = new Random();
		List<UserPhoneNumber> userPhoneNumbers = new ArrayList<UserPhoneNumber>();
		Organization destOrganization = null;
		try {
			System.out.println("JSON Object refer: "+jsonObject);
			jsonObject = new JSONObject(requestBody);
			phonenumber = jsonObject.getString("phonenumber");
			email = jsonObject.getString("email");
			orgabbr = jsonObject.getString("abbr");
			try{
			refreeEmail = jsonObject.getString("refemail");
			}
			catch(Exception e) {
				//fetch 
				Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
				List <OrganizationMembership> list = organizationMembershipService.getOrganizationMembershipListByIsAdmin(organization, true);
				refreeEmail = list.get(0).getUser().getEmail();
			}
			
			try{
				address = jsonObject.getString("address");
			}
			catch(Exception e1)
			{
				//Do nothing
			}
			try {
				name = jsonObject.getString("name");
			}
			catch(Exception e2) {
				//Do nothing
			}
			try {
				lastname = jsonObject.getString("lastname");
			}
			catch (Exception e3) {
				//Do nothing
			}
		} catch (JSONException e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "failure");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		destOrganization = organizationService.getOrganizationByAbbreviation(orgabbr);
		if ((userPhoneNumberService.findPreExistingPhoneNumber(phonenumber) == true)
				&& (userService.getUserFromEmail(email) == null)) {
			// New user scenario
			System.out.println("inside");
			User user = new User();
			user.setEmail(email);
			userService.addUser(user);
			System.out.println("user with only email added");

			UserPhoneNumber userPhoneNumber = new UserPhoneNumber();
			userPhoneNumber.setPhoneNumber(phonenumber);
			userPhoneNumber.setPrimary(true);
			userPhoneNumber.setUser(user);
			userPhoneNumberService.addUserPhoneNumber(userPhoneNumber);
			userPhoneNumbers.add(userPhoneNumber);
			user.setUserPhoneNumbers(userPhoneNumbers);
			java.util.Date date= new java.util.Date();
			Timestamp currentTimestamp= new Timestamp(date.getTime());
			user.setTime(currentTimestamp);
			user.setTextbroadcastlimit(0);
			user.setVoicebroadcastlimit(0);
			user.setWebLocale("en");
			user.setCallLocale("en");
			if(address != null)
				user.setAddress(address);
			if(name != null)
				user.setName(name);
			if(lastname != null)
				user.setLastname(lastname);
			int i = email.indexOf("@");
			String fname=email.substring(0, i);
			//password = fname+random.nextInt(1000);
			password = Integer.toString(random.nextInt(10));
			for (int count = 0; count < 9; ++count)
				password = password + random.nextInt(10);
			user.setPassToken(password);
			user.setSha256Password(passwordEncoder.encode(password));
			userService.addUser(user);
			System.out.println("user with only password added");

			User refUser = userService.getUserFromEmail(refreeEmail);
			System.out.println("User name: "+refUser.getName());
			Referral referral = new Referral();
			referral.setEmail(email);
			referral.setPhonenumber(phonenumber);
			referral.setOrganization(destOrganization);
			referral.setUser(refUser); //This is the referee
			String referralCode = String.valueOf(random.nextInt(999999));
			Referral tempReferral = referralService.getByReferralCode(referralCode);
			while (tempReferral != null) {
				referralCode = String.valueOf(random.nextInt(999999));
				tempReferral = referralService.getByReferralCode(referralCode);
			}
			referral.setReferralCode(referralCode);
			String BRANCH_KEY= "key_live_nam1TdTbBvxAcKO49YOKDnlhvzlFSU8z";
			String android_deeplink = "lokacart://";
			String android_uri_scheme = "lokacart://";
		    String android_package_name =  "com.mobile.ict.cart";
			String android_url = "https://play.google.com/store/apps/details?id=com.mobile.ict.cart";
		//	String reqBody = "{\n\"branch_key\":\""+BRANCH_KEY+"\",\n\"data\":\"{\"phonenumber\":\""+phonenumber+"\",\"email\":\""+email+"\",\"password\":\""+password+"\",\"referralCode\":\""+referralCode+"\"}\"";
			String reqBody = "{\n\"branch_key\":\""+BRANCH_KEY+"\",\n\"data\":\"{\\\"phonenumber\\\":\\\""+phonenumber+"\\\", \\\"email\\\":\\\""+email+"\\\", \\\"referralCode\\\":\\\""+referralCode+"\\\", \\\"details\\\":\\\"1\\\", \\\"token\\\":\\\""+user.getPassToken()+"\\\"}\"\n    \n}";
			
			/*String reqBody = "{\n\"branch_key\":\""+BRANCH_KEY+"\",\n\"data\":\"{\\\"phonenumber\\\":\\\""+phonenumber+
					"\\\", \\\"email\\\":\\\""+email+"\\\", \\\"referralCode\\\":\\\""+referralCode+"\\\", \\\"password\\\":\\\""+
					password+"\\\", \\\"details\\\":\\\"1\\\", \\\"token\\\":\\\""+user.getPassToken()+"\\\",\\\"organization\\\":\\\""+destOrganization.getName()+
					"\\\", \\\"abbr\\\":\\\""+destOrganization.getAbbreviation()+"\\\"}\"\n    \n}"; */
			System.out.println("body: "+reqBody);
			
			OkHttpClient client = new OkHttpClient();
			MediaType mediaType = MediaType.parse("application/json");
			okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, reqBody);
			Request request = new Request.Builder()
			  .url("https://api.branch.io/v1/url")
			  .post(body)
			  .addHeader("content-type", "application/json")
			  .addHeader("cache-control", "no-cache")
			  .addHeader("postman-token", "c366fc2b-523b-bcba-0a8b-bda6e43a2118")
			  .build();
			try {
				Response response = client.newCall(request).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				branchResponse = new JSONObject(jsonData);
				generatedUrl = branchResponse.getString("url");
				System.out.println("URL: "+generatedUrl);
				referral.setLink(generatedUrl);
				referralService.addReferral(referral);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			
			SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(destOrganization);
			String authId = smsApiKeys.getAuthId();
			String authToken = smsApiKeys.getAuthToken();
			String sourceNumber = smsApiKeys.getSourceNumber();
			RestAPI api = new RestAPI(authId, authToken, "v1");

			LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
			parameters.put("src", sourceNumber);
			parameters.put("dst", phonenumber);
			parameters.put("text",
					"Hello from Lokacart! \nYou have been referred to " + destOrganization.getName() + " by "
							+ refUser.getName() + "\nClick here: " + "http://ruralict.cse.iitb.ac.in/ruralict/app/code/"+referralCode);
			parameters.put("method", "GET");
			try {
				MessageResponse msgResponse = api.sendMessage(parameters);
				System.out.println(msgResponse);
				System.out.println("Api ID : " + msgResponse.apiId);
				System.out.println("Message : " + msgResponse.message);
				if (msgResponse.serverCode == 202) {
					System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

				} else {
					System.out.println(msgResponse.error);
				}
			} catch (PlivoException e) {
				System.out.println(e.getLocalizedMessage());
			}

			try {
				responseJsonObject.put("response", "success");
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		else{
			int flag =0;
			User user = userService.getUserFromEmail(email);
			UserPhoneNumber userPhoneNumber = userPhoneNumberService.getUserPhoneNumber(phonenumber);
			if(user == userPhoneNumber.getUser()){
				//Existing user
				int details = 0;
				if (user.getEmail() == null || user.getLastname() == null || user.getName() == null || user.getAddress() == null || user.getName() == null || user.getPincode() == null) {
					details = 1;
				}
				List<OrganizationMembership> list = user.getOrganizationMemberships();
				Iterator <OrganizationMembership> iterator = list.iterator();
				while (iterator.hasNext()) {
					OrganizationMembership organizationMembership = iterator.next();
					if (organizationMembership.getIsAdmin() == true)
						flag=1;
				}
				if (flag ==1 ) {
					try {
						responseJsonObject.put("response", "failure");
						responseJsonObject.put("reason", "Admin accounts cannot be used on the consumer app");
						return responseJsonObject.toString();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (user.getPassToken() == null) {
					//Resetting password
					password = Integer.toString(random.nextInt(10));
					for (int count = 0; count < 9; ++count)
						password = password + random.nextInt(10);
					user.setPassToken(password);
					user.setSha256Password(passwordEncoder.encode(password));
					userService.addUser(user);
					System.out.println("pass token: "+password);
				}
				List <OrganizationMembership> memberList = user.getOrganizationMemberships();
				if(memberList.isEmpty() == false){
				Iterator <OrganizationMembership> memberIterator = memberList.iterator();
				while(memberIterator.hasNext()){
					OrganizationMembership organizationMembership = memberIterator.next();
					if (destOrganization == organizationMembership.getOrganization()){
						try {
							responseJsonObject.put("response", "Already a member");
						} catch (JSONException e) {
							e.printStackTrace();
						}
						return responseJsonObject.toString();
					}
				}
				}
				User refUser = userService.getUserFromEmail(refreeEmail);
				Referral referral = new Referral();
				referral.setEmail(email);
				referral.setPhonenumber(phonenumber);
				referral.setOrganization(destOrganization);
				referral.setUser(refUser);
				String referralCode = String.valueOf(random.nextInt(999999));
				String android_url = "https://play.google.com/store/apps/details?id=com.mobile.ict.cart";
				String android_uri_scheme = "lokacart://";
			    String android_package_name =  "com.mobile.ict.cart";
				Referral tempReferral = referralService.getByReferralCode(referralCode);
				while (tempReferral != null) {
					referralCode = String.valueOf(random.nextInt(999999));
					tempReferral = referralService.getByReferralCode(referralCode);
				}
				referral.setReferralCode(referralCode);
				String BRANCH_KEY= "key_live_nam1TdTbBvxAcKO49YOKDnlhvzlFSU8z";
				String android_deeplink = "lokacart://";
				String reqBody = "{\n\"branch_key\":\""+BRANCH_KEY+"\",\n\"data\":\"{\\\"phonenumber\\\":\\\""+phonenumber+"\\\", \\\"email\\\":\\\""+email+"\\\", \\\"referralCode\\\":\\\""+referralCode+"\\\", \\\"details\\\":\\\""+details+"\\\", \\\"token\\\":\\\""+user.getPassToken()+"\\\"}\"\n    \n}";
				
				System.out.println("body: "+reqBody);							
				
				OkHttpClient client = new OkHttpClient();
				MediaType mediaType = MediaType.parse("application/json");
				okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, reqBody);
				Request request = new Request.Builder()
				  .url("https://api.branch.io/v1/url")
				  .post(body)
				  .addHeader("content-type", "application/json")
				  .addHeader("cache-control", "no-cache")
				  .addHeader("postman-token", "c366fc2b-523b-bcba-0a8b-bda6e43a2118")
				  .build(); 
			/*
			 * 	MediaType mediaType = MediaType.parse("application/json");
				okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, "{\n\"branch_key\":\"key_live_feebgAAhbH9Tv85H5wLQhpdaefiZv5Dv\",\n\"data\":\"{\\\"name\\\": \\\"Alex\\\"}\"\n    \n}");
				Request request = new Request.Builder()
				  .url("https://api.branch.io/v1/url")
				  .post(body)
				  .addHeader("content-type", "application/json")
				  .addHeader("cache-control", "no-cache")
				  .addHeader("postman-token", "7a4da102-dc81-31b9-3028-aedee4a30025")
				  .build(); */
				try {
					Response response = client.newCall(request).execute();
					System.out.println(response.toString());
				    String jsonData = response.body().string();
					branchResponse = new JSONObject(jsonData);
					generatedUrl = branchResponse.getString("url");
					System.out.println("URL: "+generatedUrl);
					referral.setLink(generatedUrl);
					referralService.addReferral(referral);
	
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				}
								
				SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(destOrganization);
				String authId = smsApiKeys.getAuthId();
				String authToken = smsApiKeys.getAuthToken();
				String sourceNumber = smsApiKeys.getSourceNumber();
				RestAPI api = new RestAPI(authId, authToken, "v1");

				LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
				parameters.put("src", sourceNumber);
				parameters.put("dst", phonenumber);
				parameters.put("text",
						"Hello from Lokacart! \nYou have been referred to " + destOrganization.getName() + " by "
								+ refUser.getName() + "\nClick here: " + "http://ruralict.cse.iitb.ac.in/ruralict/app/code/"+referralCode);
				parameters.put("method", "GET");
				System.out.println("http://ruralict.cse.iitb.ac.in/ruralict/app/code/"+referralCode);
				try {
					MessageResponse msgResponse = api.sendMessage(parameters);
					System.out.println(msgResponse);
					System.out.println("Api ID : " + msgResponse.apiId);
					System.out.println("Message : " + msgResponse.message);
					if (msgResponse.serverCode == 202) {
						System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

					} else {
						System.out.println(msgResponse.error);
					}
				} catch (PlivoException e) {
					System.out.println(e.getLocalizedMessage());
				}

				try {
					responseJsonObject.put("response", "success");
				} catch (JSONException e) {
					e.printStackTrace();
				}
					
			}
			else {
				//send error
				try {
					responseJsonObject.put("response", "failure");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}

		return responseJsonObject.toString();
	}

	
}
