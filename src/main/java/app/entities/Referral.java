package app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="referral")
public class Referral {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name = "phonenumber")
	private String phonenumber;
	
	@Column (name = "email")
	private String email;
	
	@Column (name = "referral_code")
	private String referralCode;
	
	@Column (name = "link")
	private String link;
	
	@ManyToOne
	@JoinColumn(name="organization_id")
	private Organization organization;
	
	@ManyToOne
	@JoinColumn(name="refree_id")
	private User user;
	
	public int getId(){
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getPhonenumber() {
		return this.phonenumber;
	}
	
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	
	public String getLink() {
		return this.link;
	}
	
	public void setLink(String link) {
		this.link = link;
	}

	public String getEmail() {
		return this.email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getReferralCode() {
		return this.referralCode;
	}
	
	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}
	
	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	
	public User getUser() {
		return this.user;
	}
	
	public void setUser(User user){
		this.user = user;
	}
}