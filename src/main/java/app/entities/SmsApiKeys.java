package app.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="sms_api_keys")
public class SmsApiKeys implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="api_id")
	private int apiId;
	
	@Column(name="auth_id")
	private String authId;
	
	@Column (name="auth_token")
	private String authToken;
	
	@Column (name = "source_number")
	private String sourceNumber;
	
	@OneToOne
	@JoinColumn(name="organization_id")
	private Organization organization;

	public int getApiId() {
		return this.apiId;
	}
	
	public void setApiId(int apiId) {
		this.apiId = apiId;
	}
	
	public String getAuthId() {
		return this.authId;
	}
	
	public void setAuthId(String authId) {
		this.authId = authId;
	}
	
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	
	public String getAuthToken () {
		return this.authToken;
	}
	
	public void setSourceNumber (String sourceNumber) {
		this.sourceNumber = sourceNumber;
	}
	
	public String getSourceNumber() {
		return this.sourceNumber;
	}
	
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	
	public Organization getOrganization() {
		return this.organization;
	}
}
