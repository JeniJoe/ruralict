package app.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import app.entities.Referral;

public interface ReferralRepository extends JpaRepository<Referral, Integer> {
	@Override
	public Referral findOne(Integer id);

	public Referral findByReferralCode(String referralCode);

}